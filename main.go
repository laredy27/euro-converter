package main

import(
	"log"
	"fmt"
	"flag"
	"errors"
	"encoding/xml"
	"io/ioutil"
	"net/http"
	"bitbucket.org/laredy27/euro-converter/exchange"
)

const(
	defaultSourceCurrency = "EUR"
	defaultTargetCurrency = "GBP"
)

var(
	rateFeedXML = "http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";
	currency, fromCurrency, toCurrency *string
	amount float64
)

// Cube represents a single exchange rate
type Cube struct {
	Currency string `xml:"currency,attr"`
	Rate float32 `xml:"rate,attr"`
}

// Feed ... Feed data for exchange rates
type Feed struct {
	XMLName xml.Name	`xml:"Envelope"`
	Sender string		`xml:"Sender>name"`
	Cube []Cube 		`xml:"Cube>Cube>Cube"`
}

func (f *Feed) getExchangeRates() map[string]float32 {
	rateMap := make(map[string]float32)

	for _, c := range f.Cube {
		rateMap[c.Currency] = c.Rate;
	}

	return rateMap;
}

func fetchRateFeeds() ([]byte, error){
	res, err := http.Get(rateFeedXML); if err != nil {
		return nil, err;
	}

	if res.StatusCode != 200 {
		err = errors.New("Non 2xx response returned from feed location: " + rateFeedXML)
		return nil, err
	}

	return ioutil.ReadAll(res.Body);
}

func displayRates(feed *Feed, fromCurrency string) {
	var n float32
	r := feed.getExchangeRates()

	n = 1

	for c := range r {
		if fromCurrency != c && c != defaultSourceCurrency {
			euros, err := convertToEuros(feed, fromCurrency, n); if err != nil { log.Fatal(err) } 
			amount, err := convertFromEuros(feed, c, euros); if err != nil { log.Fatal(err) } 
			fmt.Printf("%.0f %s = %.2f %s", n, fromCurrency, amount, c)
		}
		println()
	}
}

// convert amount in fromCurrency to euros
func convertToEuros(feed *Feed, fromCurrency string, amount float32) (float32, error) {
	var euros float32
	r := feed.getExchangeRates();
	xr, ok := r[fromCurrency]; if !ok {
		return 0, errors.New("Undefined currency code " + fromCurrency)
	}

	euros = amount / xr;
	return euros, nil;
}

// convert amount in euros to toCurrency 
func convertFromEuros(feed *Feed, toCurrency string, amount float32) (float32, error) {
	r := feed.getExchangeRates();
	xr, ok := r[toCurrency]; if !ok {
		return 0, errors.New("Undefined target currency code " + toCurrency)
	}

	conv := amount * xr
	return conv, nil;
}

func main() {
	fromCurrency = flag.String("fromCurrency", defaultSourceCurrency, "Specifies the source currency to convert from. The default value is EUR.")
	toCurrency = flag.String("toCurrency", defaultTargetCurrency, "Specifies the target currency to convert to. The default value is GBP.")
	flag.Parse()
	amount := flag.Arg(0)

	exchange := exchange.New(*fromCurrency, *toCurrency, amount)
	if true {
		res, err := exchange.GetRates()
		if err != nil {
			log.Fatal(err)
		}
		for _, r := range res {
			fmt.Printf("%.2f %s \n", r.Amount.Value, r.Amount.Currency.Name)
		}
	}
	
	res, err := exchange.GetRate()
	if err != nil {
		log.Fatal(err)
	}
	
	fmt.Printf("%.2f %s", res.Amount.Value, res.Amount.Currency.Name)
}