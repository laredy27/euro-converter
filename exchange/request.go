package exchange

import(
	"fmt"
	"net/http"
	"io/ioutil"
	"encoding/xml"
)

// Request -
type Request struct {
	BaseCurrency string
	URL string
}

// NewExchangeRate - NewExchangeRateRequest
func NewRequest(baseCurrency string) *Request {
	reqURL := "http://www.floatrates.com/daily/" + baseCurrency + ".xml"
	return &Request{
		BaseCurrency: baseCurrency,
		URL: reqURL,
	}
}

// NewRequest - 
func (req *Request) execute(rate *Rate) (*Rate, error) {
	res, err := http.Get(req.URL)
	if err != nil {
		return nil, fmt.Errorf(err.Error())
	}

	defer res.Body.Close()
	buf, _ := ioutil.ReadAll(res.Body)

	err = xml.Unmarshal(buf, rate); if err != nil {
		return nil, fmt.Errorf("Failed to unmarshal response into rate struct: " + err.Error())
	}

	return rate, nil
}