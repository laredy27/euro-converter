package exchange

import(
	"fmt"
	"encoding/xml"
	"strings"
	"strconv"
	"time"
)

// Exchange - Represents the data for exchange rates between currencies
type Exchange struct {
	BaseCurrency string 
	TargetCurrency string
	BaseAmount string
	Request *Request
}

// PublishedDate - 
type PublishedDate struct {
	time.Time
}

var timeLayout = "Mon, 2 Jan 2006 15:04:05 GMT"
// UnmarshalXML - 
func (date *PublishedDate) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	var i string
	d.DecodeElement(&i, &start)
	newDate, err := time.Parse(timeLayout, i)
	if err != nil {
		return err
	}

	date.Time = newDate
	return nil
}

// Rate -
type Rate struct {
	Channel xml.Name 			`xml:"channel"`
	Title string				`xml:"title"`
	BaseCurrency string 		`xml:"baseCurrency"`
	DatePublished PublishedDate	`xml:"pubDate"`
	Currencies []Currency 		`xml:"item"`
}

// Currency - 
type Currency struct {
	Name string 			`xml:"targetName"`
	Code string				`xml:"targetCurrency"`
	ExchangeRate string		`xml:"exchangeRate"`
}

// Money - 
type Money struct {
	Currency Currency
	Value float32
}

// Response - 
type Response struct {
	Rate float32
	Amount *Money
}

// New -
func New(baseCurrency, targetCurrency, baseAmount string) *Exchange {
	req := NewRequest(baseCurrency) // execute
	return &Exchange{
		BaseCurrency: baseCurrency,
		TargetCurrency: targetCurrency,
		BaseAmount: baseAmount,
		Request: req,
	}
}

// GetRate - 
func (x *Exchange) GetRate() (*Response, error) {
	rate, err := x.Request.execute(&Rate{}) // pass a rate pointer maybe
	if err != nil {
		return nil, err
	}

	for _, curr := range rate.Currencies {
		if strings.ToLower(curr.Code) == strings.ToLower(x.TargetCurrency) {
			exRate, err := parseFloat32(curr.ExchangeRate)
			if err != nil {
				return nil, err
			}
			amt, err := parseFloat32(x.BaseAmount)
			if err != nil {
				return nil, err
			}

			conv := exRate * amt

			res := &Response{
				Rate: exRate,
				Amount: &Money{
					Currency: curr,
					Value: conv,
				},
			}
			return res, nil
		}
	}
	return nil, fmt.Errorf("Unknown target currency '" + x.TargetCurrency +"' provided")
}

func (x *Exchange)GetRates() ([]*Response, error) {
	var r []*Response
	rate, err := x.Request.execute(&Rate{})

	if err != nil {
		return nil, err
	}

	for _, curr := range rate.Currencies {
		exRate, err := parseFloat32(curr.ExchangeRate)
		if err != nil {
			return nil, err
		}
		amt, err := parseFloat32(x.BaseAmount)
		if err != nil {
			return nil, err
		}

		conv := exRate * amt

		res := &Response{
			Rate: exRate,
			Amount: &Money{
				Currency: curr,
				Value: conv,
			},
		}
		r = append(r, res)
	}

	return r, nil
}

func parseFloat(s string, bitSize int) (float64, error) {
	s = strings.Replace(s, ",", "", -1)
	return strconv.ParseFloat(s, bitSize)
}

func parseFloat32(s string) (float32, error) {
	f, err := parseFloat(s, 32)

	return float32(f), err
}