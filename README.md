# Euro Currency Converter

Easily convert between various european and major world currencies. Exchange rate data provided by European Central Bank.

### Usage
`./euro-converter` Displays a list of available exchange rates

`./euro-converter -fromCurrency=USD -toCurrency=GBP {amount}` Converts a variable amount in US dollars to British pounds

`./euro-converter --help` Help

#### Flags
- fromCurrency: Specify the currency to be converted from.
- toCurrency: Specify the currency tot convert to.


### Notes
To see a list of possible exchange rates with `./euro-converter`